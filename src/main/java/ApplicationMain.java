import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ApplicationMain {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:15432/danilkabirov";

    //  Database credentials
    static final String USER = "danilkabirov";
    static final String PASS = "danilkabirov";

    // Database creation
//    CREATE DATABASE company;
//
//    CREATE TABLE Employees(id INTEGER PRIMARY KEY, age INT, first TEXT, last TEXT);
//
//    CREATE TABLE Salary(
//        id BIGINT PRIMARY KEY ,
//        employee_id INTEGER REFERENCES employees(id),
//    amount BIGINT)

    public static void main(String[] args) {
        //HOMEWORK
        addSalaries(
            List.of(1, 2, 3, 4, 5),
            List.of(108, 109, 108, 107, 107),
            List.of(100, 200, 300, 400, 500)
        );

        // LAB

//        Connection conn = null;
//        Statement stmt = null;
//        try{
//            Class.forName("org.postgresql.Driver");
//            System.out.println("Connecting to database...");
//            conn = DriverManager.getConnection(DB_URL,USER,PASS);
//            conn.setAutoCommit(false);
//
//
//            System.out.println("Creating statement...");
//            stmt = conn.createStatement(
//                ResultSet.TYPE_SCROLL_INSENSITIVE,
//                ResultSet.CONCUR_UPDATABLE);
//
//            System.out.println("Inserting one row....");
//            String SQL = "INSERT INTO Employees " +
//                "VALUES (108, 20, 'Jane', 'Eyre')";
//
//            stmt.executeUpdate(SQL);
//            // Commit data here.
//            System.out.println("Commiting data here....");
//            conn.commit();
//
//            System.out.println("Inserting one row....");
//            SQL = "INSERT INTO Employees " +
//                "VALUES (109, 20, 'David', 'Rochester')";
//
//            stmt.executeUpdate(SQL);
//
//
//            System.out.println("Inserting one row....");
//            SQL = "INSERT INTO Employees " +
//                "VALUES (110, 20, 'Rita', 'Tez')";
//
//            stmt.executeUpdate(SQL);
//
//            System.out.println("Inserting one row....");
//            String SQLstatement = "INSERT INTO Employees " +
//                "VALUES (106, 20, 'Rita', 'Tez')";
//
//            stmt.executeUpdate(SQLstatement);
//            SQLstatement = "INSERT INTO Employees " +
//                "VALUES (107, 22, 'Sita', 'Singh')";
//            stmt.executeUpdate(SQLstatement);
//
//            // Commit data here.
//            System.out.println("Commiting data here....");
//            conn.commit();
//
//            String sql = "SELECT id, first, last, age FROM Employees";
//            ResultSet rs = stmt.executeQuery(sql);
//            System.out.println("List result set for reference....");
//            printRs(rs);
//            rs.close();
//            stmt.close();
//            conn.close();
//        }catch(SQLException se){
//            //Handle errors for JDBC
//            se.printStackTrace();
//            // If there is an error then rollback the changes.
//            System.out.println("Rolling back data here....");
//            try{
//                if(conn!=null)
//                    conn.rollback();
//            }catch(SQLException se2){
//                se2.printStackTrace();
//            }//end try
//
//        }catch(Exception e){
//            //Handle errors for Class.forName
//            e.printStackTrace();
//        }finally{
//            //finally block used to close resources
//            try{
//                if(stmt!=null)
//                    stmt.close();
//            }catch(SQLException se2){
//            }
//            try{
//                if(conn!=null)
//                    conn.close();
//            }catch(SQLException se){
//                se.printStackTrace();
//            }//end finally try
//        }
//        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    static void addSalaries(List<Integer> ids, List<Integer> employee_ids, List<Integer> amount) {

        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);


            for (int i = 0; i < ids.size(); i++) {
                String SQL =
                    "INSERT INTO Salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) +
                        ")";
                stmt.executeUpdate(SQL);
            }

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            stmt.close();
            conn.close();
        } catch(SQLException se){
            se.printStackTrace();
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}
